/* eslint-disable no-undef */
import React, { useState } from 'react'
import ReactPlayer from 'react-player'
// import ReactAwesomePlayer from 'react-awesome-player'

import { Container, Row, Col } from 'react-bootstrap'

import 'bootstrap/dist/css/bootstrap.min.css';

import './App.css';

function App() {
  const [height, setHeight] = useState(window.innerHeight);

  const stream_path = 'akademy-test-stream';

  window.addEventListener('resize', () => {
    setHeight(window.innerHeight);
  })

  const path = typeof stream_path !== 'undefined'
    ? stream_path
    : window.location.pathname.split('/').pop();

  return (
    <Container fluid>
      <Row className="justify-content-md-center">
        <Col xs="12">
          <div id='content'>
            <ReactPlayer
              url={`https://akademy-cdn.kde.org/hls/${path}.m3u8`}
              playing={true}
              controls={true}
              width="100%"
              height={height}
              style={{
                width: '100%',
                height: '100%',
              }}/>
          </div>
        </Col>
      </Row>
    </Container>
  );
}

export default App;
